<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 *
 * @ORM\Table(name="reader")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 */
class Reader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=127)
     */
    private $fullname;

    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="text", length=127)
     */
    private $adress;

    /**
     * @var string
     *
     * @ORM\Column(name="passportnumber", type="string", length=127)
     */
    private $passportnumber;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return Reader
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set adress
     *
     * @param string $adress
     *
     * @return Reader
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;

        return $this;
    }

    /**
     * Get adress
     *
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * Set passportnumber
     *
     * @param string $passportnumber
     *
     * @return Reader
     */
    public function setPassportnumber($passportnumber)
    {
        $this->passportnumber = $passportnumber;

        return $this;
    }

    /**
     * Get passportnumber
     *
     * @return string
     */
    public function getPassportnumber()
    {
        return $this->passportnumber;
    }

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application", inversedBy="reader")
     */
    private $application;

    public function __construct()
    {
        $this->application = new ArrayCollection();
    }

    public function getApplication()
    {
        return $this->application;
    }


    public function __toString()
    {
        return $this->fullname ?: '';
    }

}

