<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * application
 *
 * @ORM\Table(name="application")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\applicationRepository")
 */
class Application
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="returndate", type="string", length=127)
     */
    private $returndate;

    /**
     * @var string
     *
     * @ORM\Column(name="book", type="string", length=127)
     */
    private $book;

    /**
     * @var string
     *
     * @ORM\Column(name="reader", type="string", length=127)
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Reader", mappedBy="application")
     */
    private $reader;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=127)
     */
    private $status;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="application")
     */
    private $user;

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function __toString()
    {
        return $this->book ?: '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set returndate
     *
     * @param string $returndate
     *
     * @return Application
     */
    public function setReturndate($returndate)
    {
        $this->returndate = $returndate;

        return $this;
    }


    /**
     * Get returndate
     *
     * @return string
     */
    public function getReturndate()
    {
        return $this->returndate;
    }

    /**
     * Set book
     *
     * @param string $book
     *
     * @return Application
     */
    public function setBook($book)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get book
     *
     * @return string
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * Set reader
     *
     * @param string $reader
     *
     * @return Application
     */
    public function setReader($reader)
    {
        $this->reader = $reader;

        return $this;
    }

    /**
     * Get reader
     *
     * @return string
     */
    public function getReader()
    {
        return $this->reader;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


}

